import selenium
from selenium import webdriver
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
import time

# -------------- descriptin ------------------
# This script is useful to delete all the screeing
# references of a project. The advantage is to avoid
# modifying the DB directly.
# To run this scirpt, simply write `python web_driver.py`.
# Also, make sure to put the driver in /usr/local/bin/
# However, do not forget to fill in the following CONSTANTS:
# ---------------------------------------

# the screening section, in list view, of the project of interest
URL = 'https://example.com/'
print("going to", URL)
USER = ""  # username
PSW = ""  # you can use the python os.environ("PSW")
TWOFA = True  # if your account is 2fa protected you will be asked to give the code

options = webdriver.ChromeOptions()
options.add_argument('--ignore-ssl-errors=yes')
options.add_argument('--ignore-certificate-errors')

driver = webdriver.Remote(
command_executor='http://selenium__standalone-chrome:4444/wd/hub',
options=options
)
driver.get(URL)
print("GET request sent")
time.sleep(2.1)


